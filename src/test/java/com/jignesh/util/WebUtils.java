package com.jignesh.util;

import com.jignesh.pageObjects.SignInPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by Jignesh on 05/11/2017.
 */
public class WebUtils {
    private static final String URL = "https://mail.google.com";

    public static SignInPage goToSignInPage(WebDriver driver) {
        driver.get(URL);
        return PageFactory.initElements(driver, SignInPage.class);
    }

    public static void click(WebDriver driver, By by) {
        driver.findElement(by).click();
    }

    public static void clearAndSendKeys(WebDriver driver, By by, String s) {
        driver.findElement(by).clear();
        driver.findElement(by).sendKeys(s);
    }

    public static void waitForElementTobeVisible(WebDriver driver, By by) {
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(by));
    }

    public static void waitForElementTobeClickable(WebDriver driver, By by) {
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.elementToBeClickable(by));
    }
}
