package com.jignesh.pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by Jignesh on 05/11/2017.
 */
public class EmailHomePage {

    public SignInPage signOut(WebDriver driver) {
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("a[href*='https://accounts.google.com/SignOutOptions?hl=en&continue=https://mail.google.com/mail&service=mail']")));
        WebElement profileButton = driver.findElement(By.cssSelector("a[href*='https://accounts.google.com/SignOutOptions?hl=en&continue=https://mail.google.com/mail&service=mail']"));
        profileButton.click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.linkText("Sign out")));

        WebElement signOutLink = driver.findElement(By.partialLinkText("Sign out"));
        signOutLink.click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.name("password")));
        return PageFactory.initElements(driver, SignInPage.class);

    }

    public void clickComposeButton(WebDriver driver) {
        WebDriverWait wait = new WebDriverWait(driver, 10);
        WebElement compose = driver.findElement(By.cssSelector(".T-I-KE"));
        compose.click();
        wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("textarea[name='to']")));

    }

    public void fillInRecepeint(WebDriver driver, String email) {
        WebElement toField = driver.findElement(By.cssSelector("textarea[name='to']"));
        toField.sendKeys(email);
    }

    public void fillInSubject(WebDriver driver, String subject) {
        WebElement subjectField = driver.findElement(By.name("subjectbox"));
        subjectField.sendKeys("Hello");
    }

    public void fillInBody(WebDriver driver, String s) throws InterruptedException {
        WebElement body = driver.findElement(By.cssSelector("div[aria-label='Message Body'"));
        body.clear();
        body.sendKeys(s);
        Thread.sleep(200);
    }

    public void clickSend(WebDriver driver) {
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("div[aria-label*='Send']")));
        driver.findElement(By.cssSelector("div[aria-label*='Send']")).click();
    }

    public void clickInboxWEithNewEmail(WebDriver driver, String linkText) {
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.elementToBeClickable(By.linkText(linkText)));
        driver.findElement(By.linkText(linkText)).click();
    }

    public void openNewEmail(WebDriver driver, By by) {
        driver.findElement(by).click();
    }
}
