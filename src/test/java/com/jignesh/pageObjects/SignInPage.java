package com.jignesh.pageObjects;

import com.jignesh.util.WebUtils;
import com.sun.tools.javac.tree.JCTree;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static org.openqa.selenium.support.ui.ExpectedConditions.elementToBeClickable;


public class SignInPage {

    public void fillInUserName(WebDriver driver, String email) throws InterruptedException {
        WebUtils.click(driver, By.id("identifierId"));
        WebUtils.clearAndSendKeys(driver, By.id("identifierId"), email);
        WebUtils.click(driver, By.id("identifierNext"));
        Thread.sleep(200);
    }

    public void fillInPassword(WebDriver driver, String password) throws InterruptedException {
        WebUtils.waitForElementTobeClickable(driver, By.name("password"));
        WebElement passwordField = driver.findElement(By.name("password"));
        Thread.sleep(200);
        WebUtils.clearAndSendKeys(driver, By.name("password"), password);
    }

    public EmailHomePage clickSignIn(WebDriver driver) {
        WebElement passwordNext = driver.findElement(By.id("passwordNext"));
        passwordNext.click();
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.titleContains("Inbox"));

        return PageFactory.initElements(driver, EmailHomePage.class);

    }

    public boolean isPasswordFieldDisplayed(WebDriver driver) {
        return driver.findElement(By.name("password")).isDisplayed();
    }
}
