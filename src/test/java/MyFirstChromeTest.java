import com.jignesh.pageObjects.EmailHomePage;
import com.jignesh.pageObjects.SignInPage;
import com.jignesh.util.WebUtils;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import static org.openqa.selenium.support.ui.ExpectedConditions.elementToBeClickable;


/**
 * Created by Jignesh on 28/10/2017.
 */
public class MyFirstChromeTest {
    static WebDriver driver;
    private static final String URL = "https://mail.google.com";
    private static final String EMAIL = "jigworldpay@gmail.com";
    private static final String PASSWORD = "zO43rm2Jx5Kk";

    @BeforeClass
    public static void setup(){
        System.setProperty("webdriver.chrome.driver", "/Users/Jignesh/Downloads/chromeDriver 2");
        System.setProperty("webdriver.gecko.driver", "/Users/Jignesh/Downloads/geckodriver 2");
        driver = new ChromeDriver();
        //driver = new FirefoxDriver();

    }
    @Test
    public void logIn() throws InterruptedException {
       SignInPage signInPage = WebUtils.goToSignInPage(driver);
       signInPage.fillInUserName(driver, EMAIL);
       signInPage.fillInPassword(driver, PASSWORD);
       EmailHomePage emailHomePage = signInPage.clickSignIn(driver);
       Assert.assertTrue("Inbox text should exist", driver.findElements(By.partialLinkText("Inbox")).size() > 0);
       signInPage = emailHomePage.signOut(driver);
       Assert.assertTrue("Password field should exist", signInPage.isPasswordFieldDisplayed(driver));

    }

    @Test
    public void sendAndReceiveEmail() throws InterruptedException {
        SignInPage signInPage = WebUtils.goToSignInPage(driver);
        signInPage.fillInUserName(driver, EMAIL);
        signInPage.fillInPassword(driver, PASSWORD);
        EmailHomePage emailHomePage = signInPage.clickSignIn(driver);
        emailHomePage.clickComposeButton(driver);
        emailHomePage.fillInRecepeint(driver, EMAIL);
        emailHomePage.fillInSubject(driver, "Gmail send email test");
        emailHomePage.fillInBody(driver, "Good morning");
        emailHomePage.clickSend(driver);

        //read message
        emailHomePage.clickInboxWEithNewEmail(driver, "Inbox (1)");

        //open the email
        emailHomePage.openNewEmail(driver, By.cssSelector("div[class='y6'] span[id] b"));
        //emailHomePage.clickNewEmail(driver);




    }

    @AfterClass
    public static void  tearDown(){
        driver.quit();
    }




}
